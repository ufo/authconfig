.de FN
\fI\|\\$1\|\fP
..
.TH AUTHCONFIG 8 "31 March 2010" "Red Hat, Inc."
.SH NAME
authconfig, authconfig-tui \- an interface for configuring system authentication resources
.SH SYNOPSIS
\fBauthconfig\fR
.in +7
[options] {--update|--updateall|--test|--probe|--restorebackup <name>|--savebackup <name>|--restorelastbackup}
.in -7
.SH DESCRIPTION
\fBauthconfig\fR provides a simple method of configuring
/etc/sysconfig/network to handle NIS, as well as /etc/passwd and
/etc/shadow, the files used for shadow password support.  Basic LDAP,
Kerberos 5, and Winbind client configuration is also provided.

If \fB--test\fR action is specified, \fBauthconfig\fR can be run by
users other then root, and any configuration changes are not saved but printed
instead.
If \fB--update\fR action is specified, \fBauthconfig\fR must be run by
root (or through console helper), and configuration changes are saved. Only
the files affected by the configuration changes are overwritten.
If \fB--updateall\fR action is specified, \fBauthconfig\fR must be run by
root (or through console helper), and all configuration files are written.
The \fB--probe\fP action instructs \fBauthconfig\fP to use DNS and other means
to guess at configuration information for the current host, print its guesses
if it finds them to standard output, and exit.

The \fB--restorebackup\fR, \fB--savebackup\fR, and \fB--restorelastbackup\fR
actions provide a possibility to save and later restore a backup of configuration
files which authconfig modifies. Authconfig also saves an automatic backup of
configuration files before every configuration change. This special backup can
be restored by the \fB--restorelastbackup\fR action.

If \fB--nostart\fR is specified (which is what the install program does),
ypbind or other daemons will not be started or stopped immediately following
program execution, but only enabled to start or stop at boot time. 

The \fB--enablenis\fP, \fB--enableldap\fP, \fB--enablewinbind\fP,
and \fB--enablehesiod\fP options
are used to configure user information services in \fB/etc/nsswitch.conf\fP,
the \fB--enablecache\fP option is used to configure naming services caching,
and the \fB--enableshadow\fP, \fB--enableldapauth\fP,
\fB--enablekrb5\fP, \fB--enablewinbindauth\fP,
and \fB--enablesmbauth\fP options are used to configure
authentication functions via \fB/etc/pam.d/system-auth\fP.  Each
\fB--enable\fP has a matching \fB--disable\fP option that disables the service
if it is already enabled. The respective services have parameters which configure
their server names etc.

The algorithm used for storing new password hashes can be specified by
the \fB--passalgo\fR option which takes one of the following possible values as
a parameter: \fBdescrypt\fR, \fBbigcrypt\fR, \fBmd5\fR, \fBsha256\fR, and
\fBsha512\fR.

The \fB--enablelocauthorize\fR option allows to bypass checking network
authentication services for authorization and the \fB--enablesysnetauth\fR
allows authentication of system accounts (with uid < 500) by these services.

When the configuration settings allow use of \fISSSD\fR for user information services
and authentication, \fISSSD\fR will be automatically used instead of the legacy
services and the \fISSSD\fR configuration will be set up so there is a default
domain populated with the settings required to connect the services. The \fB--enablesssd\fR
and \fB--enablesssdauth\fR options force adding \fISSSD\fR to \fB/etc/nsswitch.conf\fP
and \fB/etc/pam.d/system-auth\fP, but they do not set up the domain in the
\fISSSD\fR configuration files. The \fISSSD\fR configuration has to be
set up manually. The allowed configuration of services for \fISSSD\fR are: LDAP for
user information (\fB--enableldap\fR) and either LDAP (\fB--enableldapauth\fR), or
Kerberos (\fB--enablekrb5\fR) for authentication.

The list of options mentioned here in the manual page is not exhaustive, please
refer to \fBauthconfig --help\fR for the complete list of the options.

The \fBauthconfig-tui\fR supports all options of authconfig but it implies
\fB--update\fR as the default action. Its window contains a \fBCancel\fR
button by default. If \fB--back\fR option is specified at run time, a \fBBack\fR
button is presented instead. If \fB--kickstart\fR is specified, no interactive
screens will be seen. The values the program will use will be those specified by
the other options (\fB--passalgo\fR, \fB--enableshadow\fR, etc.).

For \fInamelist\fR you may substitute either a single name or a 
comma-separated list of names.
.PD
.SH NOTES
The \fBauthconfig-tui\fR is deprecated. No new configuration settings will be
supported by its text user interface. Use \fBsystem-config-authentication\fR GUI
application or the command line options instead.

.PD
.SH "RETURN CODES"
\fBauthconfig\fR returns 0 on success, 2 on error.

\fBauthconfig-tui\fR returns 0 on success, 2 on error, and 1 if the user cancelled
the program (by using either the \fBCancel\fR or \fBBack\fR button).

.PD
.SH FILES
.PD 0
.TP
.TP
.FN /etc/sysconfig/authconfig
Used to track whether or not particular authentication mechanisms are enabled.
Currently includes variables named USESHADOW, USEMD5, USEKERBEROS, USELDAPAUTH,
USESMBAUTH, USEWINBIND, USEWINBINDAUTH, USEHESIOD, USENIS, USELDAP, and others.
.TP
.FN /etc/passwd, /etc/shadow
Used for shadow password support.
.TP
.FN /etc/yp.conf
Configuration file for NIS support.
.TP
.FN /etc/sysconfig/network
Another configuration file for NIS support.
.TP
.FN /etc/ldap.conf
.FN /etc/openldap/ldap.conf
Used to configure LDAP (and OpenLDAP, respectively).
.TP
.FN /etc/krb5.conf
Used to configure Kerberos 5.
.TP
.FN /etc/krb.conf
Used to configure Kerberos IV (write-only).
.TP
.FN /etc/hesiod.conf
Used to configure Hesiod.
.TP
.FN /etc/pam_smb.conf
Used to configure SMB authentication.
.TP
.FN /etc/samba/smb.conf
Used to configure winbind authentication.
.TP
.FN /etc/nsswitch.conf
Used to configure user information services.
.TP
.FN /etc/pam.d/system-auth
Common PAM configuration for system services which include it using the
\fBinclude\fR directive. It is created as symlink and not relinked if
it points to another file.
.TP
.FN /etc/pam.d/system-auth-ac
Contains the actual PAM configuration for system services and is the
default target of the \fB/etc/pam.d/system-auth\fR symlink. If a local configuration
of PAM is created (and symlinked from \fBsystem-auth\fR file) this file can be \fBinclude\fRd
there.

.PD
.SH "SEE ALSO"
authconfig-gtk(8), system-auth-ac(5), passwd(5), shadow(5), pwconv(1),
domainname(1), ypbind(8), nsswitch.conf(5), smb.conf(5)

.SH AUTHORS
.nf
Nalin Dahyabhai <nalin@redhat.com>, Preston Brown <pbrown@redhat.com>,
Matt Wilson <msw@redhat.com>, Tomas Mraz <tmraz@redhat.com>
.fi
