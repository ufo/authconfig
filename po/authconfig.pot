# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2010-03-19 09:30+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../authconfig.py:81
#, python-format
msgid "usage: %s [options]"
msgstr ""

#: ../authconfig.py:88
msgid "enable shadowed passwords by default"
msgstr ""

#: ../authconfig.py:90
msgid "disable shadowed passwords by default"
msgstr ""

#: ../authconfig.py:92
msgid "enable MD5 passwords by default"
msgstr ""

#: ../authconfig.py:94
msgid "disable MD5 passwords by default"
msgstr ""

#: ../authconfig.py:97
msgid "hash/crypt algorithm for new passwords"
msgstr ""

#: ../authconfig.py:100
msgid "enable NIS for user information by default"
msgstr ""

#: ../authconfig.py:102
msgid "disable NIS for user information by default"
msgstr ""

#: ../authconfig.py:103
msgid "<domain>"
msgstr ""

#: ../authconfig.py:104
msgid "default NIS domain"
msgstr ""

#: ../authconfig.py:105 ../authconfig.py:116 ../authconfig.py:154
#: ../authconfig.py:156
msgid "<server>"
msgstr ""

#: ../authconfig.py:106
msgid "default NIS server"
msgstr ""

#: ../authconfig.py:109
msgid "enable LDAP for user information by default"
msgstr ""

#: ../authconfig.py:111
msgid "disable LDAP for user information by default"
msgstr ""

#: ../authconfig.py:113
msgid "enable LDAP for authentication by default"
msgstr ""

#: ../authconfig.py:115
msgid "disable LDAP for authentication by default"
msgstr ""

#: ../authconfig.py:117
msgid "default LDAP server hostname or URI"
msgstr ""

#: ../authconfig.py:118
msgid "<dn>"
msgstr ""

#: ../authconfig.py:119
msgid "default LDAP base DN"
msgstr ""

#: ../authconfig.py:121
msgid "enable use of TLS with LDAP (RFC-2830)"
msgstr ""

#: ../authconfig.py:123
msgid "disable use of TLS with LDAP (RFC-2830)"
msgstr ""

#: ../authconfig.py:125
msgid "enable use of RFC-2307bis schema for LDAP user information lookups"
msgstr ""

#: ../authconfig.py:127
msgid "disable use of RFC-2307bis schema for LDAP user information lookups"
msgstr ""

#: ../authconfig.py:128
msgid "<URL>"
msgstr ""

#: ../authconfig.py:129
msgid "load CA certificate from the URL"
msgstr ""

#: ../authconfig.py:132
msgid "enable authentication with smart card by default"
msgstr ""

#: ../authconfig.py:134
msgid "disable authentication with smart card by default"
msgstr ""

#: ../authconfig.py:136
msgid "require smart card for authentication by default"
msgstr ""

#: ../authconfig.py:138
msgid "do not require smart card for authentication by default"
msgstr ""

#: ../authconfig.py:139
msgid "<module>"
msgstr ""

#: ../authconfig.py:140
msgid "default smart card module to use"
msgstr ""

#: ../authconfig.py:143
msgid "action to be taken on smart card removal"
msgstr ""

#: ../authconfig.py:146
msgid "enable authentication with fingerprint readers by default"
msgstr ""

#: ../authconfig.py:148
msgid "disable authentication with fingerprint readers by default"
msgstr ""

#: ../authconfig.py:151
msgid "enable kerberos authentication by default"
msgstr ""

#: ../authconfig.py:153
msgid "disable kerberos authentication by default"
msgstr ""

#: ../authconfig.py:155
msgid "default kerberos KDC"
msgstr ""

#: ../authconfig.py:157
msgid "default kerberos admin server"
msgstr ""

#: ../authconfig.py:158 ../authconfig.py:188
msgid "<realm>"
msgstr ""

#: ../authconfig.py:159
msgid "default kerberos realm"
msgstr ""

#: ../authconfig.py:161
msgid "enable use of DNS to find kerberos KDCs"
msgstr ""

#: ../authconfig.py:163
msgid "disable use of DNS to find kerberos KDCs"
msgstr ""

#: ../authconfig.py:165
msgid "enable use of DNS to find kerberos realms"
msgstr ""

#: ../authconfig.py:167
msgid "disable use of DNS to find kerberos realms"
msgstr ""

#: ../authconfig.py:170
msgid "enable SMB authentication by default"
msgstr ""

#: ../authconfig.py:172
msgid "disable SMB authentication by default"
msgstr ""

#: ../authconfig.py:173
msgid "<servers>"
msgstr ""

#: ../authconfig.py:174
msgid "names of servers to authenticate against"
msgstr ""

#: ../authconfig.py:175
msgid "<workgroup>"
msgstr ""

#: ../authconfig.py:176
msgid "workgroup authentication servers are in"
msgstr ""

#: ../authconfig.py:179
msgid "enable winbind for user information by default"
msgstr ""

#: ../authconfig.py:181
msgid "disable winbind for user information by default"
msgstr ""

#: ../authconfig.py:183
msgid "enable winbind for authentication by default"
msgstr ""

#: ../authconfig.py:185
msgid "disable winbind for authentication by default"
msgstr ""

#: ../authconfig.py:187
msgid "security mode to use for samba and winbind"
msgstr ""

#: ../authconfig.py:189
msgid "default realm for samba and winbind when security=ads"
msgstr ""

#: ../authconfig.py:190 ../authconfig.py:192
msgid "<lowest-highest>"
msgstr ""

#: ../authconfig.py:191
msgid "uid range winbind will assign to domain or ads users"
msgstr ""

#: ../authconfig.py:193
msgid "gid range winbind will assign to domain or ads users"
msgstr ""

#: ../authconfig.py:195
msgid ""
"the character which will be used to separate the domain and user part of "
"winbind-created user names if winbindusedefaultdomain is not enabled"
msgstr ""

#: ../authconfig.py:197
msgid "the directory which winbind-created users will have as home directories"
msgstr ""

#: ../authconfig.py:199
msgid "the group which winbind-created users will have as their primary group"
msgstr ""

#: ../authconfig.py:201
msgid "the shell which winbind-created users will have as their login shell"
msgstr ""

#: ../authconfig.py:203
msgid ""
"configures winbind to assume that users with no domain in their user names "
"are domain users"
msgstr ""

#: ../authconfig.py:205
msgid ""
"configures winbind to assume that users with no domain in their user names "
"are not domain users"
msgstr ""

#: ../authconfig.py:207
msgid "configures winbind to allow offline login"
msgstr ""

#: ../authconfig.py:209
msgid "configures winbind to prevent offline login"
msgstr ""

#: ../authconfig.py:211
msgid "join the winbind domain or ads realm now as this administrator"
msgstr ""

#: ../authconfig.py:214
msgid "enable wins for hostname resolution"
msgstr ""

#: ../authconfig.py:216
msgid "disable wins for hostname resolution"
msgstr ""

#: ../authconfig.py:219
msgid "prefer dns over wins or nis for hostname resolution"
msgstr ""

#: ../authconfig.py:221
msgid "do not prefer dns over wins or nis for hostname resolution"
msgstr ""

#: ../authconfig.py:224
msgid "enable hesiod for user information by default"
msgstr ""

#: ../authconfig.py:226
msgid "disable hesiod for user information by default"
msgstr ""

#: ../authconfig.py:228
msgid "default hesiod LHS"
msgstr ""

#: ../authconfig.py:230
msgid "default hesiod RHS"
msgstr ""

#: ../authconfig.py:233
msgid "enable SSSD for user information by default"
msgstr ""

#: ../authconfig.py:235
msgid "disable SSSD for user information by default"
msgstr ""

#: ../authconfig.py:237
msgid "enable SSSD for authentication by default"
msgstr ""

#: ../authconfig.py:239
msgid "disable SSSD for authentication by default"
msgstr ""

#: ../authconfig.py:242
msgid "enable caching of user credentials in SSSD by default"
msgstr ""

#: ../authconfig.py:244
msgid "disable caching of user credentials in SSSD by default"
msgstr ""

#: ../authconfig.py:247
msgid ""
"enable caching of user information by default (automatically disabled when "
"SSSD is used)"
msgstr ""

#: ../authconfig.py:249
msgid "disable caching of user information by default"
msgstr ""

#: ../authconfig.py:252
msgid "local authorization is sufficient for local users"
msgstr ""

#: ../authconfig.py:254
msgid "authorize local users also through remote service"
msgstr ""

#: ../authconfig.py:257
msgid "check access.conf during account authorization"
msgstr ""

#: ../authconfig.py:259
msgid "do not check access.conf during account authorization"
msgstr ""

#: ../authconfig.py:262
msgid "authenticate system accounts by network services"
msgstr ""

#: ../authconfig.py:264
msgid "authenticate system accounts by local files only"
msgstr ""

#: ../authconfig.py:267
msgid "create home directories for users on their first login"
msgstr ""

#: ../authconfig.py:269
msgid "do not create home directories for users on their first login"
msgstr ""

#: ../authconfig.py:272
msgid "do not start/stop portmap, ypbind, and nscd"
msgstr ""

#: ../authconfig.py:275
msgid "do not update the configuration files, only print new settings"
msgstr ""

#: ../authconfig.py:279
msgid "display Back instead of Cancel in the main dialog of the TUI"
msgstr ""

#: ../authconfig.py:281
msgid "do not display the deprecated text user interface"
msgstr ""

#: ../authconfig.py:284
msgid "opposite of --test, update configuration files with changed settings"
msgstr ""

#: ../authconfig.py:287
msgid "update all configuration files"
msgstr ""

#: ../authconfig.py:290
msgid "probe network for defaults and print them"
msgstr ""

#: ../authconfig.py:292 ../authconfig.py:295
msgid "<name>"
msgstr ""

#: ../authconfig.py:293
msgid "save a backup of all configuration files"
msgstr ""

#: ../authconfig.py:296
msgid "restore the backup of configuration files"
msgstr ""

#: ../authconfig.py:299
msgid ""
"restore the backup of configuration files saved before the previous "
"configuration change"
msgstr ""

#: ../authconfig.py:304
msgid "unexpected argument"
msgstr ""

#: ../authconfig.py:434
msgid "Bad smart card removal action specified."
msgstr ""

#: ../authconfig.py:443
msgid "Unknown password hashing algorithm specified, using sha256."
msgstr ""

#: ../authconfig.py:476
msgid "can only be run as root"
msgstr ""

#: ../authconfig.py:492
msgid "dialog was cancelled"
msgstr ""

#: ../authconfig.py:516
#, python-format
msgid ""
"The %s file was not found, but it is required for %s support to work "
"properly.\n"
"Install the %s package, which provides this file."
msgstr ""

#: ../authconfig.py:518 ../authconfig.py:864
msgid "Warning"
msgstr ""

#: ../authconfig.py:518 ../authconfig.py:735 ../authconfig.py:742
#: ../authconfig.py:748 ../authconfig.py:757 ../authconfig.py:765
#: ../authconfig.py:806 ../authconfig.py:864
msgid "Ok"
msgstr ""

#: ../authconfig.py:522
msgid "caching"
msgstr ""

#: ../authconfig.py:523
msgid "Fingerprint reader"
msgstr ""

#: ../authconfig.py:524
msgid "Kerberos"
msgstr ""

#: ../authconfig.py:525
msgid "LDAP authentication"
msgstr ""

#: ../authconfig.py:526 ../authconfig-gtk.py:100
msgid "LDAP"
msgstr ""

#: ../authconfig.py:527 ../authconfig-gtk.py:106
msgid "NIS"
msgstr ""

#: ../authconfig.py:528
msgid "shadow password"
msgstr ""

#: ../authconfig.py:529 ../authconfig.py:531 ../authconfig-gtk.py:109
msgid "Winbind"
msgstr ""

#: ../authconfig.py:530
msgid "Winbind authentication"
msgstr ""

#: ../authconfig.py:536
msgid "User Information"
msgstr ""

#: ../authconfig.py:539
msgid "Cache Information"
msgstr ""

#: ../authconfig.py:543
msgid "Use Hesiod"
msgstr ""

#: ../authconfig.py:546
msgid "Use LDAP"
msgstr ""

#: ../authconfig.py:550
msgid "Use NIS"
msgstr ""

#: ../authconfig.py:554
msgid "Use Winbind"
msgstr ""

#: ../authconfig.py:560 ../authconfig.desktop.in.h:1
msgid "Authentication"
msgstr ""

#: ../authconfig.py:563
msgid "Use MD5 Passwords"
msgstr ""

#: ../authconfig.py:566
msgid "Use Shadow Passwords"
msgstr ""

#: ../authconfig.py:570
msgid "Use LDAP Authentication"
msgstr ""

#: ../authconfig.py:574
msgid "Use Kerberos"
msgstr ""

#: ../authconfig.py:578
msgid "Use Fingerprint reader"
msgstr ""

#: ../authconfig.py:583
msgid "Use Winbind Authentication"
msgstr ""

#: ../authconfig.py:587
msgid "Local authorization is sufficient"
msgstr ""

#: ../authconfig.py:597 ../authconfig.py:735 ../authconfig.py:742
#: ../authconfig.py:748 ../authconfig.py:757 ../authconfig.py:806
msgid "Back"
msgstr ""

#: ../authconfig.py:597 ../authconfig.py:765
msgid "Cancel"
msgstr ""

#: ../authconfig.py:598 ../authconfig.py:735 ../authconfig.py:742
#: ../authconfig.py:748 ../authconfig.py:757 ../authconfig.py:806
msgid "Next"
msgstr ""

#: ../authconfig.py:609 ../authconfig-gtk.py:600 ../authconfig.glade.h:16
msgid "Authentication Configuration"
msgstr ""

#: ../authconfig.py:732
msgid "LHS:"
msgstr ""

#: ../authconfig.py:733
msgid "RHS:"
msgstr ""

#: ../authconfig.py:734
msgid "Hesiod Settings"
msgstr ""

#: ../authconfig.py:738
msgid "Use TLS"
msgstr ""

#: ../authconfig.py:739 ../authconfig.py:746
msgid "Server:"
msgstr ""

#: ../authconfig.py:740
msgid "Base DN:"
msgstr ""

#: ../authconfig.py:741
msgid "LDAP Settings"
msgstr ""

#: ../authconfig.py:745 ../authconfig.py:800 ../authconfig.glade.h:23
msgid "Domain:"
msgstr ""

#: ../authconfig.py:747
msgid "NIS Settings"
msgstr ""

#: ../authconfig.py:751
msgid "Realm:"
msgstr ""

#: ../authconfig.py:752
msgid "KDC:"
msgstr ""

#: ../authconfig.py:753
msgid "Admin Server:"
msgstr ""

#: ../authconfig.py:754
msgid "Use DNS to resolve hosts to realms"
msgstr ""

#: ../authconfig.py:755
msgid "Use DNS to locate KDCs for realms"
msgstr ""

#: ../authconfig.py:756
msgid "Kerberos Settings"
msgstr ""

#: ../authconfig.py:760
msgid "Domain Administrator:"
msgstr ""

#: ../authconfig.py:761
msgid "Password:"
msgstr ""

#: ../authconfig.py:764
msgid "Join Settings"
msgstr ""

#: ../authconfig.py:774 ../authconfig.glade.h:43
msgid ""
"Some of the configuration changes you've made should be saved to disk before "
"continuing.  If you do not save them, then your attempt to join the domain "
"may fail.  Save changes?"
msgstr ""

#: ../authconfig.py:781
msgid "Save Settings"
msgstr ""

#: ../authconfig.py:782
msgid "No"
msgstr ""

#: ../authconfig.py:782
msgid "Yes"
msgstr ""

#. Why does your favorite shell not show up in the list?  Because it won't
#. fit, that's why!
#: ../authconfig.py:799
msgid "Security Model:"
msgstr ""

#: ../authconfig.py:801
msgid "Domain Controllers:"
msgstr ""

#: ../authconfig.py:802
msgid "ADS Realm:"
msgstr ""

#: ../authconfig.py:803
msgid "Template Shell:"
msgstr ""

#: ../authconfig.py:805
msgid "Winbind Settings"
msgstr ""

#: ../authconfig.py:807
msgid "Join Domain"
msgstr ""

#: ../authconfig.py:859
#, python-format
msgid ""
"To connect to a LDAP server with TLS protocol enabled you need a CA "
"certificate which signed your server's certificate. Copy the certificate in "
"the PEM format to the '%s' directory.\n"
"Then press OK."
msgstr ""

#. FIXME - version
#: ../authconfig.py:873
msgid ""
" <Tab>/<Alt-Tab> between elements   |   <Space> selects   |  <F12> next "
"screen"
msgstr ""

#: ../authconfig.desktop.in.h:2
msgid "Control how the system verifies users who attempt to log in"
msgstr ""

#: ../authconfig-gtk.py:97
msgid "Local accounts only"
msgstr ""

#: ../authconfig-gtk.py:103
msgid "FreeIPA"
msgstr ""

#: ../authconfig-gtk.py:118
msgid "Password"
msgstr ""

#: ../authconfig-gtk.py:121
msgid "LDAP password"
msgstr ""

#: ../authconfig-gtk.py:124
msgid "Kerberos password"
msgstr ""

#: ../authconfig-gtk.py:127
msgid "NIS password"
msgstr ""

#: ../authconfig-gtk.py:130
msgid "Winbind password"
msgstr ""

#: ../authconfig-gtk.py:425
msgid ""
"You must provide ldaps:// server address or use TLS for LDAP authentication."
msgstr ""

#: ../authconfig.glade.h:1
msgid "*"
msgstr ""

#: ../authconfig.glade.h:2
msgid "<b>Authentication Configuration</b>"
msgstr ""

#: ../authconfig.glade.h:3
msgid "<b>Local Authentication Options</b>"
msgstr ""

#: ../authconfig.glade.h:4
msgid "<b>Other Authentication Options</b>"
msgstr ""

#: ../authconfig.glade.h:5
msgid "<b>Smart Card Authentication Options</b>"
msgstr ""

#: ../authconfig.glade.h:6
msgid "<b>User Account Configuration</b>"
msgstr ""

#: ../authconfig.glade.h:7
msgid ""
"<small><b>Tip:</b> Smart cards support logging into both local and centrally "
"managed accounts.</small>"
msgstr ""

#: ../authconfig.glade.h:8
msgid ""
"<small><b>Tip:</b> This is managed via /etc/security/access.conf.</small>"
msgstr ""

#: ../authconfig.glade.h:9
msgid "Ad_min Servers:"
msgstr ""

#: ../authconfig.glade.h:10
msgid "Additional options for user information and authentication"
msgstr ""

#: ../authconfig.glade.h:11
msgid "Advanced _Options"
msgstr ""

#: ../authconfig.glade.h:12
msgid "Alert"
msgstr ""

#: ../authconfig.glade.h:13
msgid ""
"All configuration files which were modified by the previous authentication "
"configuration change will be restored from backup. Revert the changes?"
msgstr ""

#: ../authconfig.glade.h:14
msgid "Allow offline _login"
msgstr ""

#: ../authconfig.glade.h:15
msgid "Aut_hentication Method:"
msgstr ""

#: ../authconfig.glade.h:17
msgid "Card Re_moval Action:"
msgstr ""

#: ../authconfig.glade.h:18
msgid "Certificate _URL:"
msgstr ""

#: ../authconfig.glade.h:19
msgid ""
"Click this button if you did not download a CA certificate yet or you have "
"not set the CA certificate up by other means."
msgstr ""

#: ../authconfig.glade.h:20
msgid "Create _home directories on the first login"
msgstr ""

#: ../authconfig.glade.h:21
msgid "Do_n't Save"
msgstr ""

#: ../authconfig.glade.h:22
msgid "Domain _administrator:"
msgstr ""

#: ../authconfig.glade.h:24
msgid "Download CA Certificate"
msgstr ""

#: ../authconfig.glade.h:25
msgid "Enable _fingerprint reader support"
msgstr ""

#: ../authconfig.glade.h:26
msgid "Enable _local access control"
msgstr ""

#: ../authconfig.glade.h:27
msgid "Enable _smart card support"
msgstr ""

#: ../authconfig.glade.h:28
msgid ""
"Fingerprint authentication allows you to log in by scanning your finger with "
"the fingerprint reader."
msgstr ""

#: ../authconfig.glade.h:29
msgid "Hashing or crypto algorithm used for storing passwords of local users"
msgstr ""

#: ../authconfig.glade.h:30
msgid "Hostname or ldap:// or ldaps:// URI pointing to the LDAP server."
msgstr ""

#: ../authconfig.glade.h:31
msgid ""
"If the home directory of an user doesn't exist yet it will be created "
"automatically on his first login."
msgstr ""

#: ../authconfig.glade.h:32
msgid "Joining Winbind Domain"
msgstr ""

#: ../authconfig.glade.h:33
msgid "LDAP Search _Base DN:"
msgstr ""

#: ../authconfig.glade.h:34
msgid "LDAP _Server:"
msgstr ""

#: ../authconfig.glade.h:35
msgid "NIS _Domain:"
msgstr ""

#: ../authconfig.glade.h:36
msgid "NIS _Server:"
msgstr ""

#: ../authconfig.glade.h:37
msgid "R_ealm:"
msgstr ""

#: ../authconfig.glade.h:38
msgid "Require smart car_d for login"
msgstr ""

#: ../authconfig.glade.h:39
msgid ""
"Restore the configuration files backed up before the previous configuration "
"change"
msgstr ""

#: ../authconfig.glade.h:40
msgid "Revert"
msgstr ""

#: ../authconfig.glade.h:41
msgid "Set up services used for authenticating users to the system"
msgstr ""

#: ../authconfig.glade.h:42
msgid ""
"Smart card authentication allows you to log in using a certificate and key "
"associated with a smart card."
msgstr ""

#: ../authconfig.glade.h:44
msgid "Te_mplate Shell:"
msgstr ""

#: ../authconfig.glade.h:45
msgid ""
"To verify the LDAP server with TLS protocol enabled you need a CA "
"certificate which signed the server's certificate. Please fill in the URL "
"where the CA certificate in the PEM format can be downloaded from."
msgstr ""

#: ../authconfig.glade.h:46
msgid "Use DNS to _locate KDCs for realms"
msgstr ""

#: ../authconfig.glade.h:47
msgid "Use D_NS to resolve hosts to realms"
msgstr ""

#: ../authconfig.glade.h:48
msgid ""
"Use Transport Layer Security extension for LDAP as defined by RFC-2830. It "
"must not be ticked with ldaps server URI."
msgstr ""

#: ../authconfig.glade.h:49
msgid "Use _TLS to encrypt connections"
msgstr ""

#: ../authconfig.glade.h:50
msgid ""
"When enabled /etc/security/access.conf will be consulted for authorization "
"of users access."
msgstr ""

#: ../authconfig.glade.h:51
msgid "Winbind ADS R_ealm:"
msgstr ""

#: ../authconfig.glade.h:52
msgid "Winbind Domain Co_ntrollers:"
msgstr ""

#: ../authconfig.glade.h:53
msgid "Winbind _Domain:"
msgstr ""

#: ../authconfig.glade.h:54
msgid "_Download CA Certificate..."
msgstr ""

#: ../authconfig.glade.h:55
msgid "_Identity & Authentication"
msgstr ""

#: ../authconfig.glade.h:56
msgid "_Join Domain..."
msgstr ""

#: ../authconfig.glade.h:57
msgid "_KDCs:"
msgstr ""

#: ../authconfig.glade.h:58
msgid "_Password Hashing Algorithm:"
msgstr ""

#: ../authconfig.glade.h:59
msgid "_Password:"
msgstr ""

#: ../authconfig.glade.h:60
msgid "_Security Model:"
msgstr ""

#: ../authconfig.glade.h:61
msgid "_User Account Database:"
msgstr ""

#: ../authinfo.py:910 ../authinfo.py:1553 ../authinfo.py:2868
msgid "Lock"
msgstr ""

#: ../authinfo.py:910 ../authinfo.py:1555
msgid "Ignore"
msgstr ""

#: ../authinfo.py:3218
#, python-format
msgid ""
"Authentication module %s/pam_%s.so is missing. Authentication process might "
"not work correctly."
msgstr ""

#: ../authinfo.py:3761
msgid "Error downloading CA certificate"
msgstr ""
