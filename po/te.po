# translation of te.po to Telugu
# translation of te.po to
# translation of te.po to
# This file is distributed under the same license as the PACKAGE package.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER, 2006.
#
# Sree Ganesh <sthottem@redhat.com>, 2006.
# Krishna Babu K <kkrothap@redhat.com>, 2007, 2008, 2009, 2010.
msgid ""
msgstr ""
"Project-Id-Version: te\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2010-03-19 09:30+0100\n"
"PO-Revision-Date: 2010-03-26 21:22+0530\n"
"Last-Translator: Krishna Babu K <kkrothap@redhat.com>\n"
"Language-Team: Telugu <en@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"

#: ../authconfig.py:81
#, python-format
msgid "usage: %s [options]"
msgstr "వాడుక: %s [ఐచ్ఛికాలు]"

#: ../authconfig.py:88
msgid "enable shadowed passwords by default"
msgstr "అనుమతిపదాన్ని సిద్ధంగా ఉంచటం సాధ్యం"

#: ../authconfig.py:90
msgid "disable shadowed passwords by default"
msgstr "అనుమతిపదాన్ని సిద్ధంగా ఉంచటం సాధ్యంకాదు"

#: ../authconfig.py:92
msgid "enable MD5 passwords by default"
msgstr "MD5 అనుమతిపదాన్ని సిద్ధంగా ఉంచటం సాధ్యం"

#: ../authconfig.py:94
msgid "disable MD5 passwords by default"
msgstr "MD5 అనుమతిపదాన్ని సిద్ధంగా ఉంచటం సాధ్యంకాదు"

#: ../authconfig.py:97
msgid "hash/crypt algorithm for new passwords"
msgstr "కొత్త సంకేతపదాల కొరకు hash/crypt అల్గార్ధెమ్"

#: ../authconfig.py:100
msgid "enable NIS for user information by default"
msgstr "NISని వినియోగదారుని సమాచారంకోసం సిద్ధంగా ఉంచటం సాధ్యం"

#: ../authconfig.py:102
msgid "disable NIS for user information by default"
msgstr "NISని వినియోగదారుని సమాచారంకోసం సిద్ధంగా ఉంచటం సాధ్యంకాదు"

#: ../authconfig.py:103
msgid "<domain>"
msgstr "<domain>"

#: ../authconfig.py:104
msgid "default NIS domain"
msgstr "సిద్ధ NIS డొమైన్‌"

#: ../authconfig.py:105 ../authconfig.py:116 ../authconfig.py:154
#: ../authconfig.py:156
msgid "<server>"
msgstr "<server>"

#: ../authconfig.py:106
msgid "default NIS server"
msgstr "సిద్ధ NIS సర్వరు"

#: ../authconfig.py:109
msgid "enable LDAP for user information by default"
msgstr "LDAP సిద్ధ వినియోగదారుని సమాచారం కోసం సాధ్యం"

#: ../authconfig.py:111
msgid "disable LDAP for user information by default"
msgstr "LDAP సిద్ధ వినియోగదారుని సమాచారం కోసం సాధ్యంకాదు"

#: ../authconfig.py:113
msgid "enable LDAP for authentication by default"
msgstr "LDAP సిద్ధ అధికారంకోసం సాధ్యం"

#: ../authconfig.py:115
msgid "disable LDAP for authentication by default"
msgstr "LDAP సిద్ధ అధికారంకోసం సాధ్యంకాదు"

#: ../authconfig.py:117
msgid "default LDAP server hostname or URI"
msgstr "అప్రమేయ LDAP సేవిక హోస్టుపేరు లేదా URI"

#: ../authconfig.py:118
msgid "<dn>"
msgstr "<dn>"

#: ../authconfig.py:119
msgid "default LDAP base DN"
msgstr "సిద్ధ LDAP ఆధారిత DN"

#: ../authconfig.py:121
msgid "enable use of TLS with LDAP (RFC-2830)"
msgstr "TLS వుపయోగమును LDAP ద్వారా చేతనంచేయుము (RFC-2830)"

#: ../authconfig.py:123
msgid "disable use of TLS with LDAP (RFC-2830)"
msgstr "TLS వుపయోగమును LDAPతో అచేతనము చేయుము (RFC-2830)"

#: ../authconfig.py:125
msgid "enable use of RFC-2307bis schema for LDAP user information lookups"
msgstr "LDAP వినియోగదారి సమాచారపు లుకప్స్ కొరకు RFC-2307bis స్కీమా యొక్క వుపయోగాన్ని చేతనముచేయి"

#: ../authconfig.py:127
msgid "disable use of RFC-2307bis schema for LDAP user information lookups"
msgstr "LDAP వినియోగదారి సమాచార లుకప్స్ కొరకు RFC-2307bis స్కీమా యొక్క వుపయోగాన్ని అచేతనముచేయి"

#: ../authconfig.py:128
msgid "<URL>"
msgstr "<URL>"

#: ../authconfig.py:129
msgid "load CA certificate from the URL"
msgstr "URLనుండీ లోడ్ CA ధృవీకరణ పత్రం"

#: ../authconfig.py:132
msgid "enable authentication with smart card by default"
msgstr "స్మార్టు కార్డుతో సిద్ధంగా అధికారాన్ని పొందటం సాధ్యం"

#: ../authconfig.py:134
msgid "disable authentication with smart card by default"
msgstr "స్మార్టు కార్డుతో సిద్ధంగా అధికారాన్ని పొందటం సాధ్యంకాదు"

#: ../authconfig.py:136
msgid "require smart card for authentication by default"
msgstr "సిద్ధంగా ధృవీకరణాకు స్మార్టుకార్డు కావాలి"

#: ../authconfig.py:138
msgid "do not require smart card for authentication by default"
msgstr "సిద్ధంగా ధృవీకరణాకు స్మార్టుకార్డు అవసరంలేదు"

#: ../authconfig.py:139
msgid "<module>"
msgstr "<module>"

#: ../authconfig.py:140
msgid "default smart card module to use"
msgstr "వినియోగించటానికి సిద్ధ స్మార్టుకార్డు గుణకం"

#: ../authconfig.py:143
msgid "action to be taken on smart card removal"
msgstr "స్మార్టుకార్డు తొలగించటానికి తీసుకొనబడిన చర్య"

#: ../authconfig.py:146
msgid "enable authentication with fingerprint readers by default"
msgstr "అప్రమేయంగా వ్రేలిముద్ర చదువరులతో దృవీకరణ పొందుటను చేతనంచేయుము"

#: ../authconfig.py:148
msgid "disable authentication with fingerprint readers by default"
msgstr "అప్రమేయంగా వ్రేలిముద్ర చదువరులతో దృవీకరణ పొందుటను అచేతనంచేయుము"

#: ../authconfig.py:151
msgid "enable kerberos authentication by default"
msgstr "కర్బెరోస్ ధృవీకరణను సిద్ధంగా చేయగలదు"

#: ../authconfig.py:153
msgid "disable kerberos authentication by default"
msgstr "కెర్బెరోస్ ధృవీకరణను సిద్ధంగా చేయలేదు"

#: ../authconfig.py:155
msgid "default kerberos KDC"
msgstr "సిద్ధ కెర్బెరోస్ KDC"

#: ../authconfig.py:157
msgid "default kerberos admin server"
msgstr "సిద్ధ కెర్బెరోస్ నిర్వహణ సర్వరు"

#: ../authconfig.py:158 ../authconfig.py:188
msgid "<realm>"
msgstr "<realm>"

#: ../authconfig.py:159
msgid "default kerberos realm"
msgstr "సిద్ధ కెర్బెరోస్ realm"

#: ../authconfig.py:161
msgid "enable use of DNS to find kerberos KDCs"
msgstr "కెర్బెరోస్ KDCsను కనుగొనటానికి DNS ఉపయోగించగలదు"

#: ../authconfig.py:163
msgid "disable use of DNS to find kerberos KDCs"
msgstr "కెర్బెరోస్ KDCsను కనుగొనటానికి DNS ఉపయోగించలేదు"

#: ../authconfig.py:165
msgid "enable use of DNS to find kerberos realms"
msgstr "DNSను కెర్బెరోస్ realmsను కనుగొనటానికి వీలవుతుంది"

#: ../authconfig.py:167
msgid "disable use of DNS to find kerberos realms"
msgstr "DNSను కెర్బెరోస్ realmsను కనుగొనటానికి వీలవదు"

#: ../authconfig.py:170
msgid "enable SMB authentication by default"
msgstr "SMB ధృవీకరణను సిద్ధంగా చేయగలదు"

#: ../authconfig.py:172
msgid "disable SMB authentication by default"
msgstr "SMB ధృవీకరణను సిద్ధంగా చేయలేదు"

#: ../authconfig.py:173
msgid "<servers>"
msgstr "<servers>"

#: ../authconfig.py:174
msgid "names of servers to authenticate against"
msgstr "వ్యతిరేకంగా ధృవీకరించవలసిన సర్వర్ల పేర్లు"

#: ../authconfig.py:175
msgid "<workgroup>"
msgstr "<workgroup>"

#: ../authconfig.py:176
msgid "workgroup authentication servers are in"
msgstr "కార్యసమూహాల ధృవీకరణ సర్వర్లు"

#: ../authconfig.py:179
msgid "enable winbind for user information by default"
msgstr "అప్రమేయంగా వినియోగదారుని సమాచారము కొరకు విన్‌బెండ్‌ను చేతనముచేయండి"

#: ../authconfig.py:181
msgid "disable winbind for user information by default"
msgstr "సిద్ధంగా వినియోగదారుని సమాచారం కోసం విన్‌బైండ్ ఉపకరించదు "

#: ../authconfig.py:183
msgid "enable winbind for authentication by default"
msgstr "సిద్ధంగా వినియోగదారుని సమాచారం కోసం విన్‌బైండ్ ఉపకరిస్తుంది"

#: ../authconfig.py:185
msgid "disable winbind for authentication by default"
msgstr "సిద్ధంగా వినియోగదారుని సమాచారం కోసం విన్‌బైండ్ ఉపకరిస్తుంది"

#: ../authconfig.py:187
msgid "security mode to use for samba and winbind"
msgstr "సాంబా మరియూ విన్ బిండ్ ఉపయోగించటానికి రక్షణ రీతి"

#: ../authconfig.py:189
msgid "default realm for samba and winbind when security=ads"
msgstr "రక్షణ=ads గా ఉన్నప్పుడు సాంబ మరియూ విన్ బిన్ కోసం సిద్ధ realm"

#: ../authconfig.py:190 ../authconfig.py:192
msgid "<lowest-highest>"
msgstr "<lowest-highest>"

#: ../authconfig.py:191
msgid "uid range winbind will assign to domain or ads users"
msgstr "uid స్థాయి విన్‌బైండ్ డొమైన్‌ లేదా ads వినియోగదారులకి అనుసంధిస్తుంది"

#: ../authconfig.py:193
msgid "gid range winbind will assign to domain or ads users"
msgstr "gid స్థాయి విన్‌బైండ్ డొమైన్‌ లేదా ads వినియోగదారులకి అనుసంధిస్తుంది"

#: ../authconfig.py:195
msgid ""
"the character which will be used to separate the domain and user part of "
"winbind-created user names if winbindusedefaultdomain is not enabled"
msgstr ""
"క్షేత్రాన్ని మరియూ వినియోగదారుని winbind-created వినియోగ నామాలను వేరుచేయటానికి ఉపయోగించబడుతుందో అది "
"ఒకవేళ winbindusedefaultdomain ఉపయోగానికి పనికిరాకపోతే ఉపయోగపడుతుంది"

#: ../authconfig.py:197
msgid "the directory which winbind-created users will have as home directories"
msgstr "winbind-created వినియోగదారులు హోం డైరెక్టరీ కలిగి ఉండాలి"

#: ../authconfig.py:199
msgid "the group which winbind-created users will have as their primary group"
msgstr "winbind-created వినియోగదారుల సమూహం వాళ్ల ప్రాధమిక సమూహాన్ని కలిగి ఉండాలి"

#: ../authconfig.py:201
msgid "the shell which winbind-created users will have as their login shell"
msgstr "winbind-created వినియోగదారుల షల్ వాళ ప్రవేశ షల్ లాంటిదాన్ని కలిగి ఉంటుంది"

#: ../authconfig.py:203
msgid ""
"configures winbind to assume that users with no domain in their user names "
"are domain users"
msgstr ""
"విన్‌బైండ్‌ని వినియోగదారులను ఏ క్షేత్రమూ వాళ్ల వినియీగదారుల పేర్లలో లేనట్లు అవి క్షేత్ర వినియోగదారులుగా కానట్లు "
"ఆకృతీకరించండి"

#: ../authconfig.py:205
msgid ""
"configures winbind to assume that users with no domain in their user names "
"are not domain users"
msgstr ""
"విన్‌బైండ్‌ని వినియోగదారులను ఏ క్షేత్రమూ వాళ్ల వినియీగదారుల పేర్లలో లేనట్లు అవి క్షేత్ర వినియోగదారులుగా ఉన్నట్లు "
"ఆకృతీకరించండి"

#: ../authconfig.py:207
msgid "configures winbind to allow offline login"
msgstr "విండ్ బైండ్ ఆకృతీకరణ ఆఫ్ లైన్ లాగిన్ అనుమతినిస్తుంది"

#: ../authconfig.py:209
msgid "configures winbind to prevent offline login"
msgstr "ఆఫ్ లైన్ లాగిన్ నియంత్రించుటకు విండ్ బైండ్ ను ఆకృతీకరించు"

#: ../authconfig.py:211
msgid "join the winbind domain or ads realm now as this administrator"
msgstr "విన్‌బైండ్ డొమైన్‌ లేదా ads realmని నిర్వాహకుడిగా కలపండి"

#: ../authconfig.py:214
msgid "enable wins for hostname resolution"
msgstr "winలను ఆతిధేయ తీర్మానాలకు ఉపయోగించవచ్చు"

#: ../authconfig.py:216
msgid "disable wins for hostname resolution"
msgstr "winలను ఆతిధేయ తీర్మానాలకు ఉపయోగించలేము"

#: ../authconfig.py:219
msgid "prefer dns over wins or nis for hostname resolution"
msgstr "హోస్టుపేరు రెజొల్యూషన్ కొరకు wins లేదా nis పైన dns యెంచుము"

#: ../authconfig.py:221
msgid "do not prefer dns over wins or nis for hostname resolution"
msgstr "హోస్టునామము రెజొల్యూషన్ కొరకు wins లేదా nis పైన dns యెంచవద్దు"

#: ../authconfig.py:224
msgid "enable hesiod for user information by default"
msgstr "hesiodని వినియోగదారుని సమాచారంకోసం సిద్ధంగా ఉపయోగించవచ్చు"

#: ../authconfig.py:226
msgid "disable hesiod for user information by default"
msgstr "hesiodని వినియోగదారుని సమాచారంకోసం సిద్ధంగా ఉపయోగించలేము"

#: ../authconfig.py:228
msgid "default hesiod LHS"
msgstr "సిద్ధ hesiod LHS"

#: ../authconfig.py:230
msgid "default hesiod RHS"
msgstr "సిద్ధ hesiod RHS"

#: ../authconfig.py:233
msgid "enable SSSD for user information by default"
msgstr "అప్రమేయంగా వినియోగదారి సమాచారము కొరకు SSSDను చేతనముచేయి"

#: ../authconfig.py:235
msgid "disable SSSD for user information by default"
msgstr "అప్రమేయంగా వినియోగదారి సమాచారము కొరకు SSSDను అచేతనముచేయి"

#: ../authconfig.py:237
msgid "enable SSSD for authentication by default"
msgstr "అప్రమేయంగా ధృవీకరణ కొరకు SSSD చేతనముచేయి"

#: ../authconfig.py:239
msgid "disable SSSD for authentication by default"
msgstr "అప్రమేయంగా ధృవీకరణ కొరకు SSSDను అచేతనముచేయి"

#: ../authconfig.py:242
msgid "enable caching of user credentials in SSSD by default"
msgstr "SSSD నందు అప్రమేయంగా వినియోగదారి వివరములను క్యాచింగ్‌ చేయుట చేతనముచేయి"

#: ../authconfig.py:244
msgid "disable caching of user credentials in SSSD by default"
msgstr "SSSD నందు అప్రమేయంగా వినియోగదారి వివరముల యొక్క క్యాచింగ్‌ను అచేతనముచేయి"

#: ../authconfig.py:247
msgid ""
"enable caching of user information by default (automatically disabled when "
"SSSD is used)"
msgstr ""
"అప్రమేయంగా వినియోగదారి సమాచారము యొక్క క్యాచింగ్‌ను చేతనముచేయి (SSSD వుపయోగించబడునప్పుడు స్వయంచాలకంగా "
"అచేతనము చేయబడింది)"

#: ../authconfig.py:249
msgid "disable caching of user information by default"
msgstr "వినియోగదారుని సమాచారాన్ని సిద్ధంగా caching చేయటం వీలవ్వదు"

#: ../authconfig.py:252
msgid "local authorization is sufficient for local users"
msgstr "స్థానిక వినియోగదారులకు స్థానికారం సరిపోతుంది"

#: ../authconfig.py:254
msgid "authorize local users also through remote service"
msgstr "అధికార పూర్వక స్థానిక వినియోగదారులు దూరస్థ సేవలద్వారా కూడా ఉండవచ్చు"

#: ../authconfig.py:257
msgid "check access.conf during account authorization"
msgstr "ఖాతా దృవీకరణలో access.conf ను పరిశీలించు"

#: ../authconfig.py:259
msgid "do not check access.conf during account authorization"
msgstr "ఖాతా దృవీకరణలో access.conf ను పరిశీలించవద్దు"

#: ../authconfig.py:262
msgid "authenticate system accounts by network services"
msgstr "నెట్వర్కు సేవలద్వారా ధృవీకృత కంప్యూటరు ఖాతాలు"

#: ../authconfig.py:264
msgid "authenticate system accounts by local files only"
msgstr "స్థానిక ఫైళ్లద్వారా మాత్రమే ధృవీకృత కంప్యూటరు ఖాతాలు"

#: ../authconfig.py:267
msgid "create home directories for users on their first login"
msgstr "వినియోగదారులకు వారి మొదటి లాగిన్ నందు నివాస సంచయాలను సృష్టించుము"

#: ../authconfig.py:269
msgid "do not create home directories for users on their first login"
msgstr "వినియోగదారులకు వారి మొదటి లాగిన్ నందు నివాస సంచయాలను సృష్టించవద్దు"

#: ../authconfig.py:272
msgid "do not start/stop portmap, ypbind, and nscd"
msgstr "పోర్టుమాప్పును ప్రారంభం/ఆపటం చేయకు, ypbind, మరియూ nscd"

#: ../authconfig.py:275
msgid "do not update the configuration files, only print new settings"
msgstr "ఆకృతీకరణ ఫైళ్లను నవీకరించకు, కొత్త అమర్పులను మాత్రమే ముద్రించు"

#: ../authconfig.py:279
msgid "display Back instead of Cancel in the main dialog of the TUI"
msgstr "TUI ప్రధాన డైలాగులో రద్దుకు బదులు వెనుకను ప్రదర్శించు"

#: ../authconfig.py:281
msgid "do not display the deprecated text user interface"
msgstr "తగ్గిన పాఠ వినియోగదారుని అంతర్ముఖీనతను ప్రదర్శించకు"

#: ../authconfig.py:284
msgid "opposite of --test, update configuration files with changed settings"
msgstr "--పాఠ వ్యతిరేకం ఆకృతీకరణ ఫైలును మార్చిన అమర్పులతో నవీకరించు"

#: ../authconfig.py:287
msgid "update all configuration files"
msgstr "అన్ని ఆకృతీకరణ ఫైళ్లనూ నవీకరించు"

#: ../authconfig.py:290
msgid "probe network for defaults and print them"
msgstr "సిద్ధాలకు probe network మరియూ వాటిని ముద్రించు"

#: ../authconfig.py:292 ../authconfig.py:295
msgid "<name>"
msgstr "<నామము>"

#: ../authconfig.py:293
msgid "save a backup of all configuration files"
msgstr "అన్ని ఆకృతీకరణ దస్త్రాలను బ్యాక్అప్ తీయుము"

#: ../authconfig.py:296
msgid "restore the backup of configuration files"
msgstr "ఆకృతీకరణ దస్త్రాల బ్యాక్అప్‌ను తిరిగివుంచుము"

#: ../authconfig.py:299
msgid ""
"restore the backup of configuration files saved before the previous "
"configuration change"
msgstr "గత ఆకృతీకరణ మార్పుకు ముందుగా దాసిన ఆకృతీకరణ దస్త్రాల యొక్క బ్యాక్అప్‌ను తిరిగివుంచుము"

#: ../authconfig.py:304
msgid "unexpected argument"
msgstr "ఊహించని వాదన"

#: ../authconfig.py:434
msgid "Bad smart card removal action specified."
msgstr "చెడ్డ స్మార్టు కార్డు తొలగింపు క్రియ తెలుపబడిం."

#: ../authconfig.py:443
msgid "Unknown password hashing algorithm specified, using sha256."
msgstr "తెలియని సంకేతపదము హాషింగ్ అల్గార్ధెమ్ తెలుపబడింది, sha256 ఉపయోగిస్తూ."

#: ../authconfig.py:476
msgid "can only be run as root"
msgstr "రూటుగా మాత్రమే నడుస్తుంది"

#: ../authconfig.py:492
msgid "dialog was cancelled"
msgstr "డైలాగ్ రద్దయ్యింది"

#: ../authconfig.py:516
#, python-format
msgid ""
"The %s file was not found, but it is required for %s support to work "
"properly.\n"
"Install the %s package, which provides this file."
msgstr ""
"ఈ %s ఫైలు కనుగొనబడలేదు, కానీ ఇది సరిగా పనిచేయటానికి %s మద్దతుకావాలి.\n"
"ఏది ఈ ఫైలును సమకూర్చుతుందో, ఆ %s ప్యాకేజీని సంస్థాపించండి."

#: ../authconfig.py:518 ../authconfig.py:864
msgid "Warning"
msgstr "హెచ్చరిక"

#: ../authconfig.py:518 ../authconfig.py:735 ../authconfig.py:742
#: ../authconfig.py:748 ../authconfig.py:757 ../authconfig.py:765
#: ../authconfig.py:806 ../authconfig.py:864
msgid "Ok"
msgstr "సరే"

#: ../authconfig.py:522
msgid "caching"
msgstr "చేజింగు"

#: ../authconfig.py:523
msgid "Fingerprint reader"
msgstr "వ్రేలిముద్ర చదువరి"

#: ../authconfig.py:524
msgid "Kerberos"
msgstr "కెర్బెరోస్"

#: ../authconfig.py:525
msgid "LDAP authentication"
msgstr "LDAP దృవీకరణ"

#: ../authconfig.py:526 ../authconfig-gtk.py:100
msgid "LDAP"
msgstr "LDAP"

#: ../authconfig.py:527 ../authconfig-gtk.py:106
msgid "NIS"
msgstr "NIS"

#: ../authconfig.py:528
msgid "shadow password"
msgstr "షాడో అనుమతిపదం"

#: ../authconfig.py:529 ../authconfig.py:531 ../authconfig-gtk.py:109
msgid "Winbind"
msgstr "విన్‌బిండ్"

#: ../authconfig.py:530
msgid "Winbind authentication"
msgstr "విన్‌బైండ్ ధృవీకరణ"

#: ../authconfig.py:536
msgid "User Information"
msgstr "వినియోగదారును సమాచారం"

#: ../authconfig.py:539
msgid "Cache Information"
msgstr "Cache సమాచారం"

#: ../authconfig.py:543
msgid "Use Hesiod"
msgstr "Hesiodని ఉపయోగించు"

#: ../authconfig.py:546
msgid "Use LDAP"
msgstr "LDAPని వినియోగించు"

#: ../authconfig.py:550
msgid "Use NIS"
msgstr "NISని వినియోగించు"

#: ../authconfig.py:554
msgid "Use Winbind"
msgstr "విన్‌బైండ్‌ని వినియోగించు"

#: ../authconfig.py:560 ../authconfig.desktop.in.h:1
msgid "Authentication"
msgstr "ధృవీకరణ"

#: ../authconfig.py:563
msgid "Use MD5 Passwords"
msgstr "MD5 అనుమతిపదాలను ఉపయోగించు"

#: ../authconfig.py:566
msgid "Use Shadow Passwords"
msgstr "షాడో అనుమతిపదాలను ఉపయోగించు"

#: ../authconfig.py:570
msgid "Use LDAP Authentication"
msgstr "LDAP ధృవీకరణలను ఉపయోగించు"

#: ../authconfig.py:574
msgid "Use Kerberos"
msgstr "కెర్బెరోసును వినియోగించు"

#: ../authconfig.py:578
msgid "Use Fingerprint reader"
msgstr "వ్రేలిముద్ర చదువరిని వుపయోగించుము"

#: ../authconfig.py:583
msgid "Use Winbind Authentication"
msgstr "విన్ బిండ్ ధృవీకరణను ఉపయోగించు"

#: ../authconfig.py:587
msgid "Local authorization is sufficient"
msgstr "స్థానిక ధృవీకరణ చాలినంత ఉంది"

#: ../authconfig.py:597 ../authconfig.py:735 ../authconfig.py:742
#: ../authconfig.py:748 ../authconfig.py:757 ../authconfig.py:806
msgid "Back"
msgstr "వెనుక"

#: ../authconfig.py:597 ../authconfig.py:765
msgid "Cancel"
msgstr "రద్దు"

#: ../authconfig.py:598 ../authconfig.py:735 ../authconfig.py:742
#: ../authconfig.py:748 ../authconfig.py:757 ../authconfig.py:806
msgid "Next"
msgstr "తరువాత"

#: ../authconfig.py:609 ../authconfig-gtk.py:600 ../authconfig.glade.h:16
msgid "Authentication Configuration"
msgstr "ప్రామాణీకరణ యొక్క ధృవీకరణ"

#: ../authconfig.py:732
msgid "LHS:"
msgstr "LHS:"

#: ../authconfig.py:733
msgid "RHS:"
msgstr "RHS:"

#: ../authconfig.py:734
msgid "Hesiod Settings"
msgstr "Hesiod అమర్పులు"

#: ../authconfig.py:738
msgid "Use TLS"
msgstr "TLSను వినియోగించు"

#: ../authconfig.py:739 ../authconfig.py:746
msgid "Server:"
msgstr "సర్వరు:"

#: ../authconfig.py:740
msgid "Base DN:"
msgstr "మూల DN:"

#: ../authconfig.py:741
msgid "LDAP Settings"
msgstr "LDAP అమర్పులు"

#: ../authconfig.py:745 ../authconfig.py:800 ../authconfig.glade.h:23
msgid "Domain:"
msgstr "డొమైన్‌:"

#: ../authconfig.py:747
msgid "NIS Settings"
msgstr "NIS అమర్పులు"

#: ../authconfig.py:751
msgid "Realm:"
msgstr "Realm:"

#: ../authconfig.py:752
msgid "KDC:"
msgstr "KDC:"

#: ../authconfig.py:753
msgid "Admin Server:"
msgstr "నిర్వహణ సర్వరు:"

#: ../authconfig.py:754
msgid "Use DNS to resolve hosts to realms"
msgstr "DNSను realms ఆతిధేయాలను తొలగించటానికి ఉపయోగించు"

#: ../authconfig.py:755
msgid "Use DNS to locate KDCs for realms"
msgstr "DNSను realms కోసం KDCsను స్థాపించటానికి ఉపయోగించు"

#: ../authconfig.py:756
msgid "Kerberos Settings"
msgstr "కెర్బెరోస్ అమర్పులు"

#: ../authconfig.py:760
msgid "Domain Administrator:"
msgstr "క్షేత్ర నిర్వాహకుడు:"

#: ../authconfig.py:761
msgid "Password:"
msgstr "అనుమతిపదం:"

#: ../authconfig.py:764
msgid "Join Settings"
msgstr "మేళన అమర్పులు"

#: ../authconfig.py:774 ../authconfig.glade.h:43
msgid ""
"Some of the configuration changes you've made should be saved to disk before "
"continuing.  If you do not save them, then your attempt to join the domain "
"may fail.  Save changes?"
msgstr ""
"మీరు చేసిన కొన్ని ఆకృతీకరణ మార్పులు మీరు కొనసాగించటానికి ముందే భద్రపరచవలసి ఉంది.  మీరు వాటిని "
"భద్రపరచకపోతే, డొమైన్‌లో చేరటానికి మీ ప్రయత్నం విఫలం కావచ్చు.  మార్పులను భద్రపరచండి?"

#: ../authconfig.py:781
msgid "Save Settings"
msgstr "అమర్పులను భద్రపరువు"

#: ../authconfig.py:782
msgid "No"
msgstr "కాదు"

#: ../authconfig.py:782
msgid "Yes"
msgstr "అవును"

#. Why does your favorite shell not show up in the list?  Because it won't
#. fit, that's why!
#: ../authconfig.py:799
msgid "Security Model:"
msgstr "రక్షణ రీతి:"

#: ../authconfig.py:801
msgid "Domain Controllers:"
msgstr "క్షేత్ర నియంత్రణలు:"

#: ../authconfig.py:802
msgid "ADS Realm:"
msgstr "ADS Realm:"

#: ../authconfig.py:803
msgid "Template Shell:"
msgstr "Template Shell:"

#: ../authconfig.py:805
msgid "Winbind Settings"
msgstr "విన్‌బైండ్ అమర్పులు"

#: ../authconfig.py:807
msgid "Join Domain"
msgstr "డొమైన్‌ చేర్చుము"

#: ../authconfig.py:859
#, python-format
msgid ""
"To connect to a LDAP server with TLS protocol enabled you need a CA "
"certificate which signed your server's certificate. Copy the certificate in "
"the PEM format to the '%s' directory.\n"
"Then press OK."
msgstr ""
"LDAP సర్వరుకు TLS ప్రోటోకాలుతో అనుసంధించటం సాధ్యం మీకు మీ సర్వరు ధృవీకరణను చేసే ఒక CA "
"ధృవీకరణ పత్రం కావలసి ఉంది. ధృవీకరణ పత్రాన్ని PEM ఆకృతిలో '%s' డైరెక్టరీకి కాపీ చేయండి.\n"
"Then press OK."

#. FIXME - version
#: ../authconfig.py:873
msgid ""
" <Tab>/<Alt-Tab> between elements   |   <Space> selects   |  <F12> next "
"screen"
msgstr " <Tab>/<Alt-Tab> మూలకాల మధ్య   |   <Space> ఎన్నికలు   |  <F12> తరువాతి తెర"

#: ../authconfig.desktop.in.h:2
msgid "Control how the system verifies users who attempt to log in"
msgstr "ఎవరు ప్రవేశానికి ప్రయత్నిస్తున్నారో కంప్యూటరు ఎలా తనిఖీచేస్తుందో నియంత్రించు "

#: ../authconfig-gtk.py:97
msgid "Local accounts only"
msgstr "స్థానిక ఖాతాలు మాత్రమే"

#: ../authconfig-gtk.py:103
msgid "FreeIPA"
msgstr "FreeIPA"

#: ../authconfig-gtk.py:118
msgid "Password"
msgstr "సంకేతపదము"

#: ../authconfig-gtk.py:121
msgid "LDAP password"
msgstr "LDAP సంకేతపదము"

#: ../authconfig-gtk.py:124
msgid "Kerberos password"
msgstr "కేర్బరోస్ సంకేతపదము"

#: ../authconfig-gtk.py:127
msgid "NIS password"
msgstr "NIS సేకతపదము"

#: ../authconfig-gtk.py:130
msgid "Winbind password"
msgstr "విన్‌బైండ్ సంకేతపదము"

#: ../authconfig-gtk.py:425
msgid ""
"You must provide ldaps:// server address or use TLS for LDAP authentication."
msgstr ""
"LDAP దృవీకరణ కొరకు మీరు తప్పక ldaps:// సర్వర్ చిరునామాను అందించాలి లేదా TLS వుపయోగించాలి."

#: ../authconfig.glade.h:1
msgid "*"
msgstr "*"

#: ../authconfig.glade.h:2
msgid "<b>Authentication Configuration</b>"
msgstr "<b>ధృవీకరణ ఆకృతీకరణ</b>"

#: ../authconfig.glade.h:3
msgid "<b>Local Authentication Options</b>"
msgstr "<b>స్థానిక ధృవీకరణ ఐచ్చికములు</b>"

#: ../authconfig.glade.h:4
msgid "<b>Other Authentication Options</b>"
msgstr "<b>ఇతర ధృవీకరణ ఐచ్చికములు</b>"

#: ../authconfig.glade.h:5
msgid "<b>Smart Card Authentication Options</b>"
msgstr "<b>స్మార్ట్ కార్డ్ దృవీకరణ ఐచ్చికములు</b>"

#: ../authconfig.glade.h:6
msgid "<b>User Account Configuration</b>"
msgstr "<b>వినియోగదారి ఖాతా ఆకృతీకరణ</b>"

#: ../authconfig.glade.h:7
msgid ""
"<small><b>Tip:</b> Smart cards support logging into both local and centrally "
"managed accounts.</small>"
msgstr ""
"<small><b>చిట్కా:</b> స్థానికమైన వాటిలోకి మరియు కేంద్రీయంగా నిర్వరించు ఖాతాలలోనికి లాగిన్ అవుటను స్మార్ట్ "
"కార్డ్స్ మద్దతించును.</small>"

#: ../authconfig.glade.h:8
msgid ""
"<small><b>Tip:</b> This is managed via /etc/security/access.conf.</small>"
msgstr ""
"<small><b>చిట్కా:</b> ఇది /etc/security/access.conf. ద్వారా నిర్వహించబడును.</small>"

#: ../authconfig.glade.h:9
msgid "Ad_min Servers:"
msgstr "నిర్వహణ సర్వర్లు (_m):"

#: ../authconfig.glade.h:10
msgid "Additional options for user information and authentication"
msgstr "వినియోగదారి సమాచారము మరియు ధృవీకరణ కొరకు అదనపు ఐచ్చికములు"

#: ../authconfig.glade.h:11
msgid "Advanced _Options"
msgstr "అధునాతన ఐచ్చికములు (_O)"

#: ../authconfig.glade.h:12
msgid "Alert"
msgstr "అప్రమత్తం"

#: ../authconfig.glade.h:13
msgid ""
"All configuration files which were modified by the previous authentication "
"configuration change will be restored from backup. Revert the changes?"
msgstr ""
"గత దృవీకరణ ఆకృతీకరణము మార్పుతో సవరించబడిన అన్ని ఆకృతీకరణ దస్త్రాలు బ్యాక్అప్ నుండి తిరిగివుంచ "
"బడినవి. మార్పులను తిప్పివుంచాలా?"

#: ../authconfig.glade.h:14
msgid "Allow offline _login"
msgstr "ఆఫ్‌లైన్ లాగిన్‌ను అనుమతించుము (_l)"

#: ../authconfig.glade.h:15
msgid "Aut_hentication Method:"
msgstr "ధృవీకరణ విధానము (_h):"

#: ../authconfig.glade.h:17
msgid "Card Re_moval Action:"
msgstr "కార్డు తొలగింపు చర్య (_m):"

#: ../authconfig.glade.h:18
msgid "Certificate _URL:"
msgstr "ధృవీకరణపత్రము _URL:"

#: ../authconfig.glade.h:19
msgid ""
"Click this button if you did not download a CA certificate yet or you have "
"not set the CA certificate up by other means."
msgstr ""
"మీరు CA దృవీకరణపత్రమును యిప్పటివరకు దిగుమతి చేయకపోయినా లేక యితరత్రా యేవిధంగాను CA ధృవీకరణపత్రమును "
"అమర్చకపోయినా యీ బటన్‌ను నొక్కుము."

#: ../authconfig.glade.h:20
msgid "Create _home directories on the first login"
msgstr "మొదటి లాగిన్ నందు నివాస సంచయాలను సృష్టించుము(_h)"

#: ../authconfig.glade.h:21
msgid "Do_n't Save"
msgstr "భద్రపరవకు (_n)"

#: ../authconfig.glade.h:22
msgid "Domain _administrator:"
msgstr "డొమైన్ నిర్వహణాధికారి (_a):"

#: ../authconfig.glade.h:24
msgid "Download CA Certificate"
msgstr "CA ధృవీకరణ పత్రాన్ని దిగుమతిచేయి"

#: ../authconfig.glade.h:25
msgid "Enable _fingerprint reader support"
msgstr "వ్రేలిముద్ర చదువరి మద్దతును చేతనముచేయుము (_f)"

#: ../authconfig.glade.h:26
msgid "Enable _local access control"
msgstr "స్థానిక యాక్సెస్ నియంత్రణను చేతనము చేయి (_l)"

#: ../authconfig.glade.h:27
msgid "Enable _smart card support"
msgstr "స్మార్టు కార్డు మద్దతును చేతనము చేయుము (_s)"

#: ../authconfig.glade.h:28
msgid ""
"Fingerprint authentication allows you to log in by scanning your finger with "
"the fingerprint reader."
msgstr ""
"వ్రేలిముద్ర చదువరితో మీ వ్రేలును స్కాను చేయుట ద్వారా వ్రేలిముద్ర దృవీకరణ మిమ్ములను లాగిన్ అవ్వుటకు "
"అనుమతిస్తుంది."

#: ../authconfig.glade.h:29
msgid "Hashing or crypto algorithm used for storing passwords of local users"
msgstr "స్థానిక వినియోగదారుల యొక్క సంకేతపదములను నిల్వవుంచుటకు వుపయోగించు హాషింగ్ లేదా క్రిప్టో అల్గార్దెమ్"

#: ../authconfig.glade.h:30
msgid "Hostname or ldap:// or ldaps:// URI pointing to the LDAP server."
msgstr "హోస్టుపేరు లేదా ldap:// లేదా ldaps:// URI అనునది LDAP సేవికను సూచిస్తుంది."

#: ../authconfig.glade.h:31
msgid ""
"If the home directory of an user doesn't exist yet it will be created "
"automatically on his first login."
msgstr ""
"ఒక వినియోగదారుని నివాస సంచయం ఇంకా లేకపోతే అది అతని మొదటి లాగిన్ నందు స్వయంచాలకంగా సృష్టించబడుతుంది."

#: ../authconfig.glade.h:32
msgid "Joining Winbind Domain"
msgstr "విన్‌బైండ్ డొమైన్‌లో చేరుతోంది"

#: ../authconfig.glade.h:33
msgid "LDAP Search _Base DN:"
msgstr "LDAP సెర్చ్ బేస్ DN (_B):"

#: ../authconfig.glade.h:34
msgid "LDAP _Server:"
msgstr "LDAP సర్వరు (_S):"

#: ../authconfig.glade.h:35
msgid "NIS _Domain:"
msgstr "NIS డొమైన్ (_D):"

#: ../authconfig.glade.h:36
msgid "NIS _Server:"
msgstr "NIS సర్వరు (_S):"

#: ../authconfig.glade.h:37
msgid "R_ealm:"
msgstr "రియాల్మ్ (_e):"

#: ../authconfig.glade.h:38
msgid "Require smart car_d for login"
msgstr "లాగిన్ కొరకు స్మార్ట్ కార్డ్ అవసరము (_d)"

#: ../authconfig.glade.h:39
msgid ""
"Restore the configuration files backed up before the previous configuration "
"change"
msgstr "గత ఆకృతీకరణము మార్పుకు ముందుగా బ్యాక్అప్ వుంచిన ఆకృతీకరణ దస్త్రాలను తిరిగివుంచుము"

#: ../authconfig.glade.h:40
msgid "Revert"
msgstr "తిప్పివుంచు"

#: ../authconfig.glade.h:41
msgid "Set up services used for authenticating users to the system"
msgstr "సిస్టమ్‌కు వినియోగదారులను ధృవీకరించుటకు వుపయోగించు సేవలను అమర్చుము"

#: ../authconfig.glade.h:42
msgid ""
"Smart card authentication allows you to log in using a certificate and key "
"associated with a smart card."
msgstr ""
"Smart కార్డు ధృవీకరణ smart కార్డుతో కూడి యున్న ధృవీకరణ లేదా కీకి ప్రవేశించటానికి ఉపయోగించటానికి "
"అనుమతిస్తుంది."

#: ../authconfig.glade.h:44
msgid "Te_mplate Shell:"
msgstr "మాదిరి(టెంప్లేట్) షెల్ (_m):"

#: ../authconfig.glade.h:45
msgid ""
"To verify the LDAP server with TLS protocol enabled you need a CA "
"certificate which signed the server's certificate. Please fill in the URL "
"where the CA certificate in the PEM format can be downloaded from."
msgstr ""
"LDAP సర్వరుకు TLS ప్రోటోకాలుతో అనుసంధించటం సాధ్యం మీకు మీ సర్వరు ధృవీకరణను చేసే ఒక CA "
"ధృవీకరణ పత్రం కావలసి ఉంది. PEM రూపంలోని CA దృవీకరణ పత్రము ఎక్కడనుండి దిగుమతి చేయగలమో దయచేసి "
"URL నింపండి."

#: ../authconfig.glade.h:46
msgid "Use DNS to _locate KDCs for realms"
msgstr "DNSని KDCలను ఉంచటానికి realmsకోసం ఉపయోగించు(_l)"

#: ../authconfig.glade.h:47
msgid "Use D_NS to resolve hosts to realms"
msgstr "హోస్టులను రియాల్మ్స్‍కు నిష్కృతి చేయుటకు D_NS వుపయోగించుము"

#: ../authconfig.glade.h:48
msgid ""
"Use Transport Layer Security extension for LDAP as defined by RFC-2830. It "
"must not be ticked with ldaps server URI."
msgstr ""
"RFC-2830 చేత నిర్వచించబడినట్లుగా ట్రాన్సుపోర్టు లేయర్ సెక్యూరిటీ పొడిగింపును LDAP కొరకు "
"వుపయోగించుము. అది ldaps URIతో టిక్ కాబడరాదు."

#: ../authconfig.glade.h:49
msgid "Use _TLS to encrypt connections"
msgstr "TLSని encrypt అనుసంధానాలకు ఉపయోగించు(_T)"

#: ../authconfig.glade.h:50
msgid ""
"When enabled /etc/security/access.conf will be consulted for authorization "
"of users access."
msgstr ""
"/etc/security/access.conf ను చేతనం చేసినప్పుడు వినియోగదారుల వాడుక దృవీకరణ కొరకు "
"సంప్రదించబడుతుంది."

#: ../authconfig.glade.h:51
msgid "Winbind ADS R_ealm:"
msgstr "విన్‌బైండ్ ADS రియాల్మ్ (_e):"

#: ../authconfig.glade.h:52
msgid "Winbind Domain Co_ntrollers:"
msgstr "విన్‌బైండ్ డొమైన్ నియంత్రికలు (_n):"

#: ../authconfig.glade.h:53
msgid "Winbind _Domain:"
msgstr "విన్‌బైండ్ క్షేత్రము (_D):"

#: ../authconfig.glade.h:54
msgid "_Download CA Certificate..."
msgstr "CA ధృవీకరణ పత్రాన్ని దిగుమతిచేయి... (_D)"

#: ../authconfig.glade.h:55
msgid "_Identity & Authentication"
msgstr "గుర్తించు & ధృవీకరణ (_I)"

#: ../authconfig.glade.h:56
msgid "_Join Domain..."
msgstr "డొమైన్ చేర్చుము... (_J)"

#: ../authconfig.glade.h:57
msgid "_KDCs:"
msgstr "_KDCs:"

#: ../authconfig.glade.h:58
msgid "_Password Hashing Algorithm:"
msgstr "సంకేతపదము హాషింగ్ అల్గార్దెమ్ (_P):"

#: ../authconfig.glade.h:59
msgid "_Password:"
msgstr "సంకేతపదము (_P):"

#: ../authconfig.glade.h:60
msgid "_Security Model:"
msgstr "రక్షణ రీతి (_S):"

#: ../authconfig.glade.h:61
msgid "_User Account Database:"
msgstr "వినియోగదారి ఖాతా డాటాబేస్ (_U):"

#: ../authinfo.py:910 ../authinfo.py:1553 ../authinfo.py:2868
msgid "Lock"
msgstr "లాక్"

#: ../authinfo.py:910 ../authinfo.py:1555
msgid "Ignore"
msgstr "వదిలేయి"

#: ../authinfo.py:3218
#, python-format
msgid ""
"Authentication module %s/pam_%s.so is missing. Authentication process might "
"not work correctly."
msgstr "ధృవీకృత గుణకం %s/pam_%s.so తప్పిపోయింది. ధృవీకరణ విధానం సరిగ్గా పనిచేస్తూ వుండకపోవచ్చు."

#: ../authinfo.py:3761
msgid "Error downloading CA certificate"
msgstr "CA ధృవీకరణ పత్రాన్ని దిగుమతిచేయటంలో దోషం"
